"""Some functions for my pulsar analysis, mainly with PINT"""
import numpy as np
import scipy.optimize as op
from scipy import stats
import matplotlib.pyplot as plt
from pint.eventstats import hm, hmw, h2sig
import pyfits


def vhe_pulsed_prob(phss, template, dc, verbose=True):
    """
    See 1FHL, section 4.5, for details. Returns p-value of pulsation.
    """
    def likeli(x, phss, template, xtemplate, dc):
        if x < 0 or x > 1:
            return 1000000.
        else:
            probs = np.interp(phss, xtemplate, template, right=template[0])
            return -np.log(1-x + x*(probs-dc)/(1-dc)).sum()

    x0 = [0.5]
    template_xvalues = np.arange(0, 1, 1./template.size)
    result = op.minimize(likeli, x0, args=(phss.astype(np.float64),
                                           template.astype(np.float64),
                                           template_xvalues, dc),
                         options={'disp': verbose})
    ts = 2*(-result['fun'])
    pvalue = (1.-stats.chi2.cdf(ts, 1))/2.
    sigma = np.sqrt(stats.chi2.ppf(1-pvalue, 1))
    if verbose:
        print("TS:", ts, "p-value:", pvalue, "sigma:", sigma)
    return pvalue


def single_profile_likelihood(dphs, phases, template, xtemplate, weights):
    """
    A single likelihood calculation for a given pulse template and phases
    shifted by "dphs". Likelihood is calculated as per eqn 2 in Pletsch & Clark
    2015. -ln(Likelihood) is returned for minimization purpose.
    """
    phss = phases.astype(np.float64) + dphs
    # ensure that all the phases are within 0-1
    phss[phss > 1.0] -= 1.0
    phss[phss < 0.0] += 1.0
    probs = np.interp(phss, xtemplate, template, right=template[0])
    if weights is None:
        return -np.log(probs).sum()
    else:
        return -np.log(weights*probs + 1.0 - weights).sum()


def profile_likelihood(dphs, phases, template, xtemplate, weights):
    """
    Likelihood calculations for a given pulse template and phases shifted by
    the values in "dphs". Likelihood is calculated as per eqn 2 in
    Pletsch & Clark 2015.
    """
    trials = phases.astype(np.float64) + dphs[:, np.newaxis]
    # ensure that all the phases are within 0-1
    trials[trials > 1.0] -= 1.0
    trials[trials < 0.0] += 1.0
    probs = np.interp(trials, xtemplate, template, right=template[0])
    if weights is None:
        return (np.log(probs)).sum(axis=1)
    else:
        return (np.log(weights*probs + 1.0 - weights)).sum(axis=1)


def marginalize_over_phase(
        phases, template, weights=None, resolution=1.0/1024, minimize=True,
        showplot=False, error=False, lophs=0.0, hiphs=1.0):
    """
    Match given pulse template to combined photon phases. A maximum likelihood
    technique is used.

    Parameters
    ----------
    error : boolean, optional, default: False
       Instead of the value of the maximum likelihood, the middle of
       the 1-sigma range will be returned together with the 1-sigma error.

    Return
    ------
    The shift and the max log likehood are returned.
    If "error" is True, "minimize" will be als set to True and the shift, the
    max likelihood and the error according to Kerr et al. 2015 are returned.
    """
    ltemp = len(template)
    xtemp = np.arange(ltemp) * 1.0/ltemp
    minimize = True if error is True else minimize
    if minimize:
        phs, like = marginalize_over_phase(phases, template, weights,
                                           resolution=1.0/64, minimize=False,
                                           showplot=showplot)
        hwidth = 0.03
        lophs, hiphs = phs - hwidth, phs + hwidth
        result = op.minimize(single_profile_likelihood, [phs],
                             args=(phases, template, xtemp, weights),
                             bounds=[[lophs, hiphs]], method='L-BFGS-B')
        if error:
            # look for 1 sigma errors
            phs = result['x'][0]
            while (single_profile_likelihood(phs, phases, template, xtemp,
                                             weights) - result['fun'])*2 < 1.0:
                phs += 0.00001
            hiphs = phs
            phs = result['x'][0]
            while (single_profile_likelihood(phs, phases, template, xtemp,
                                             weights) - result['fun'])*2 < 1.0:
                phs -= 0.00001
            lophs = phs
            return lophs+(hiphs-lophs)/2., -result['fun'], (hiphs-lophs)/2.
        return result['x'], -result['fun']
    dphss = np.arange(lophs, hiphs, resolution)
    lnlikes = profile_likelihood(dphss, phases, template, xtemp, weights)
    if showplot:
        plt.plot(dphss, lnlikes)
        plt.xlabel("Pulse Phase")
        plt.ylabel("Log likelihood")
        plt.show()
    return dphss[lnlikes.argmax()], lnlikes.max()


def slide_over(
        phases, template, weights=None, resolution=1.0/1024, error=False):
    """
    Match given pulse template to combined photon phases. A maximum likelihood
    technique is used.

    Parameters
    ----------
    error : boolean, optional, default: False
       Instead of the value of the maximum likelihood, the middle of
       the 1-sigma range will be returned together with the 1-sigma error.

    Return
    ------
    The shift and the max log likehood are returned.
    If "error" is True, "minimize" will be als set to True and the shift, the
    max likelihood and the error according to Kerr et al. 2015 are returned.
    """
    ltemp = len(template)
    xtemp = np.arange(ltemp) * 1.0/ltemp
    if error:
        phs, likes = slide_over(phases, template, weights,
                                resolution=1.0/64, error=False)
        phs = phs[likes.argmax()]
        hwidth = 0.03
        lophs, hiphs = phs - hwidth, phs + hwidth
        result = op.minimize(single_profile_likelihood, [phs],
                             args=(phases, template, xtemp, weights),
                             bounds=[[lophs, hiphs]], method='L-BFGS-B')
        # look for 1 sigma errors
        phs = result['x'][0]
        while (single_profile_likelihood(phs, phases, template, xtemp,
                                         weights) - result['fun'])*2 < 1.0:
            phs += 0.0001
        hiphs = phs
        phs = result['x'][0]
        while (single_profile_likelihood(phs, phases, template, xtemp,
                                         weights) - result['fun'])*2 < 1.0:
            phs -= 0.0001
        lophs = phs
        return lophs+(hiphs-lophs)/2., (hiphs-lophs)/2., \
            result['x'][0], result['fun']
    dphss = np.arange(0, 1., resolution)
    lnlikes = profile_likelihood(dphss, phases, template, xtemp, weights)
    return dphss, lnlikes


def prof_vs_weights(phases, weights, nbins=50, **kwargs):
    """
    Show binned profiles (and H-test/sigma values) as a function of the minimum
    weight used.

    Parameters
    ----------
    nbins : int, optional, default: 50
        Number of bins in the profile plots.
    use_weights : boolean, optional, default: True
        If true, use weights for the h-test calculations.
    minWeights : list, optional, default: [.1, .2, .3, .4, .5, .6, .7, .8, .9]
        The first 9 elements of the list will be plotted.
    psrName : string, optional, default: 'Default PSR'
        For the title of the plot.
    output : string, optional
        If given, the plots will be saved as 'output'_profs.png and
        'output'_htest.png (a "weighted" is added if 'use_weights' is True).
    showPlots : boolean, optional, default: True
        If True, the plots will be displayed.
    """
    minW = kwargs.get('minWeights', [0., .1, .2, .3, .4, .5, .7, .8, .9])
    uw = kwargs.get('use_weights', True)
    psrName = kwargs.get('psrName', "Default PSR")
    # A smart way to figure out the best number of plots ...
    # sqrtBins = np.sqrt(len(minW))
    # if sqrtBins < (np.ceil(sqrtBins) - 0.5):
    #     ncols = int(np.ceil(sqrtBins))
    #     nrows = int(sqrtBins)
    # else:
    #     ncols = int(sqrtBins)
    #     nrows = int(sqrtBins)

    ncols = 3
    nrows = 3
    f, ax = plt.subplots(nrows, ncols, sharex=True)
    htests = []
    for ii, minwgt in enumerate(minW[:9]):
        good = weights > minwgt
        nphotons = np.sum(good)
        wgts = weights[good] if uw else None
        if nphotons == 0:
            hval = 0
        elif uw:
            hval = hmw(phases[good], weights=wgts)
        else:
            hval = hm(phases[good])
        hval = 1e-10 if hval <= 0 else hval
        htests.append(hval)
        c = ii % ncols
        r = ii / nrows
        ax[r][c].hist(phases[good], nbins, range=[0, 1], weights=wgts,
                      color='k', histtype='step')
        ax[r][c].set_title("%.2f / %.1f(%.1f) / %.0f" %
                           (minwgt, hval, h2sig(hval), nphotons),
                           fontsize=11)
        if c == 0:
            ax[r][c].set_ylabel("Counts")
            if uw:
                ax[r][c].set_ylabel("Weighted counts")
        if r == nrows - 1:
            ax[r][c].set_xlabel("Phase")
        f.suptitle("%s: Minwgt / H-test(sigma) / Approx # events" %
                   psrName, fontweight='bold')
    # plt.close()

    g = plt.figure(2)
    plt.plot(minW, htests, 'k')
    plt.xlabel("Minimum weight")
    plt.ylabel("H-test")
    plt.title(psrName)

    if kwargs.get('showPlot', True):
        plt.show()
    if 'output' in kwargs:
        out = kwargs.get('output') + \
              ("_profs_weighted.png" if uw else "_profs.png")
        f.savefig(out)
        out = kwargs.get('output') + \
            ("_htest_weighted.png" if uw else "_htest.png")
        g.savefig(out)
    # plt.close()


def getData(
        inFile, weightCol=None, mjdRange=[0, np.inf], eRange=[0, np.inf],
        minWeight=0, phsField='PULSE_PHASE', tdbField='BARY_TIME'):
    """
    Get mjds, phases and energy out of a FT1 file. Optionally also weights and
    tdbs.

    Parameters
    ----------

    Return
    ------
    Returns a dict with np arrays. Keys are 'mjd', 'phase', 'energy',
    'tdb', 'weight' and 'conversion'
    """
    data = dict()
    # Load photon times from FT1 file
    hdulist = pyfits.open(inFile)
    ft1hdr = hdulist[1].header
    ft1dat = hdulist[1].data

    # Collect TIMEZERO and MJDREF
    try:
        timezero = np.longdouble(ft1hdr['TIMEZERO'])
    except KeyError:
        timezero = np.longdouble(ft1hdr['TIMEZERI']) + \
                   np.longdouble(ft1hdr['TIMEZERF'])
    print("TIMEZERO = {0}".format(timezero))
    try:
        mjdref = np.longdouble(ft1hdr['MJDREF'])
    except KeyError:
        # Here I have to work around an issue where the MJDREFF key is stored
        # as a string in the header and uses the "1.234D-5" syntax for floats,
        # which is not supported by Python
        if isinstance(ft1hdr['MJDREFF'], basestring):
            mjdref = np.longdouble(ft1hdr['MJDREFI']) + \
                     np.longdouble(ft1hdr['MJDREFF'].replace('D', 'E'))
        else:
            mjdref = np.longdouble(ft1hdr['MJDREFI']) + \
                     np.longdouble(ft1hdr['MJDREFF'])
    print("MJDREF = {0}".format(mjdref))
    data['mjd'] = np.array(ft1dat.field('TIME'),
                           dtype=np.longdouble) / 86400.0 + mjdref + timezero
    data['energy'] = np.array(ft1dat.field('ENERGY'), dtype=np.longdouble)
    data['phase'] = np.array(ft1dat.field(phsField), dtype=np.longdouble)
    data['ra'] = np.array(ft1dat.field('RA'), dtype=np.longdouble)
    data['dec'] = np.array(ft1dat.field('DEC'), dtype=np.longdouble)
    data['conversion'] = np.array(ft1dat.field('CONVERSION_TYPE'),
                                  dtype=np.int)
    data['weight'] = None
    if weightCol is not None:
        data['weight'] = np.array(ft1dat.field(weightCol))
        idx = (data['weight'] > minWeight) & \
              (data['mjd'] > mjdRange[0]) & (data['mjd'] < mjdRange[1]) & \
              (data['energy'] > eRange[0]) & (data['energy'] < eRange[1])
        data['weight'] = data['weight'][idx]
        # make the highest weight = 1, but keep min weight the same
        # wmx, wmn = weights.max(), weights.min()
        # weights = wmn + ((weights - wmn) * (1.0 - wmn) / (wmx - wmn))
        # print "There are %d events, with min / max weights %.3f / %.3f" % \
        #       (len(weights), wmn, wmx)
    else:
        idx = (data['mjd'] > mjdRange[0]) & (data['mjd'] < mjdRange[1]) & \
              (data['energy'] > eRange[0]) & (data['energy'] < eRange[1])
    data['phase'] = data['phase'][idx]
    data['mjd'] = data['mjd'][idx]
    data['energy'] = data['energy'][idx]
    data['ra'] = data['ra'][idx]
    data['dec'] = data['dec'][idx]
    data['conversion'] = data['conversion'][idx]
    try:
        data['tdb'] = np.array(ft1dat.field(tdbField),
                               dtype=np.longdouble) / 86400.0 + \
                       mjdref + timezero
        data['tdb'] = data['tdb'][idx]
    except KeyError:
        data['tdb'] = None

    return data


def getRaDec(infile):
    """
    Get RA, DEC from the center of the ROI.

    Parameters
    ----------

    Return
    ------
    Two floats
    """

    hdulist = pyfits.open(infile)
    ft1hdr = hdulist[1].header

    # Get center of ROI
    roi_ra = float(ft1hdr['DSVAL3'].split(',')[0].split('(')[1])
    roi_dec = float(ft1hdr['DSVAL3'].split(',')[1])

    return roi_ra, roi_dec
