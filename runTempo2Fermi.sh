#!/bin/bash

if [ "$1" == "-h" ] || [ "$1" == "--help" ] || [ $# -ne 2 ]; then
    echo " Usage: $0 <fits file> <par file>"
    exit -1
fi

input=$1
par=$2
spacecraft_file="/media/david/Data/Fermi/enrico/Data/download/lat_spacecraft_merged.fits"

tempo2 -gr fermi -phase -bary -f ${par} -ft1 ${input} -ft2 ${spacecraft_file} -allInfo
