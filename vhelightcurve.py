#!/usr/bin/env python

import numpy as np
from pylab import MaxNLocator
from scipy import stats
import argparse
from pulsartiming import getData, getRaDec
from pulsartiming import vhe_pulsed_prob
from pint.eventstats import hmw, h2sig
import itemplate
import templatereader
import matplotlib.pyplot as plt
from astropy.coordinates import SkyCoord
import astropy.units as u
import sys

if __name__ == '__main__':
    # Parse command line arguments
    desc = "Plot VHE pulse profile, weighted by the profile at lower energies"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("eventfile", help="Fermi event FITS file name. " +
                        "Must be phased.")
    parser.add_argument("--weightcol", help="Column name for event weights.",
                        default=None)
    parser.add_argument("--minweight", help="Minimum weight to include in " +
                        "the low energy profile.", default=-1, type=float)
    parser.add_argument("--minMJD", help="Minimum MJD to include in analysis",
                        default=0, type=float)
    parser.add_argument("--maxMJD", help="Maximum MJD to include in analysis",
                        default=np.inf, type=float)
    parser.add_argument("--lnbins", default=50, type=int,
                        help="Number of bins in the profile (default 50)")
    parser.add_argument("--hnbins", default=50, type=int,
                        help="Number of bins in the profile (default 50)")
    parser.add_argument("--outfile", help="Output file name for figures.",
                        default=None)
    parser.add_argument("--phasecol", help="Column name for phases (default " +
                        "PULSE_PHASE).", default='PULSE_PHASE')
    parser.add_argument("--loe", help="Energy bin for the low energy " +
                        "profile (default 1000 50000).", default='1000 50000')
    parser.add_argument("--hie", help="Energy mins for the VHE profile [MeV]" +
                        "(default 10,25,50 GeV).", default='10000 25000 50000')
    parser.add_argument("--maxe", type=float,
                        help="Maximum energy for vhe profile.", default=1.e20)
    parser.add_argument("--radfront", help="Extraction radius for the VHE " +
                        "pulse profile, front converting events (default 0.5, "
                        "~95%% containment at 10GeV)", default=0.5, type=float)
    parser.add_argument("--radback", help="Extraction radius for the VHE " +
                        "pulse profile, back converting events (default 1.0, "
                        "~95%% containment at 10GeV)", default=1.0, type=float)
    parser.add_argument("--template", help="Pulse profile template for low " +
                        "energy profile (default templateVHE.txt).",
                        default="templateVHE.txt")
    parser.add_argument('--length', help="Interpolated points for template " +
                        "(default 10000).", type=int, default=10000)
    parser.add_argument('--shift',  type=float, default=0,
                        help="Shift phases if problems with the fitter.")
    parser.add_argument('--text', help="optional text in plot (default: '')",
                        default="")
    parser.add_argument('--postext', help="Position of text given in " +
                        "fraction of the axes (x y, default: 0.05 0.9)",
                        default='0.05 0.9')
    parser.add_argument("--nolegend", help="Do not plot legend(default:False)",
                        default=False, action="store_true")
    parser.add_argument('--skewed',  action='store_true', default=False,
                        help="Do you want to fit skewed Gaussians?")
    parser.add_argument('--cols', type=str,
                        default="crimson yellowgreen royalblue yellogreen",
                        help="The colors for the high energy photons")
    parser.add_argument('--mc', type=int, default=0, help="# of mc " +
                        " simulations to verify the P threshold")
    parser.add_argument('--threshold', type=float, default=0, help=" The " +
                        "P threshold to evaluate false positives.")
    args = parser.parse_args()
    args.loe = args.loe.split()
    args.hie = args.hie.split()
    args.cols = args.cols.split()

    # Get low energy photons
    dloe = getData(
            args.eventfile, weightCol=args.weightcol, phsField=args.phasecol,
            mjdRange=[args.minMJD, args.maxMJD], minWeight=args.minweight,
            eRange=[float(args.loe[0]), float(args.loe[1])])
    print(" Extracted %i events for the low energy photons!" %
          dloe['phase'].size)
    if dloe['weight'] is None:
        dloe['weight'] = np.ones(dloe['phase'].size)
    # Make sure phases are between 0 and 1
    if any(dloe['phase']) < 0:
        print(" ! WARNING: I found negative phases!")
    # Apply the desired shift for the fit
    if args.shift != 0:
        dloe['phase'] = np.where(dloe['phase'] > (1-args.shift),
                                 dloe['phase']-(1-args.shift),
                                 dloe['phase']+args.shift)

    # Give significance of low energy pulse profile
    h = float(hmw(dloe['phase'], dloe['weight']))
    print("Htest : {0:.2f} ({1:.2f} sigma)".format(h, h2sig(h)))

    # Get template from file or do a fit and write it to a file
    try:
        template = templatereader.read_gaussfitfile(args.template, args.length)
        print("Got template from %s ..." % args.template)
        template /= template.sum()*(1./args.length)  # be sure of normalization
        itemplate.light_curve(dloe['phase'], weights=dloe['weight'],
                              nbins=args.lnbins)
        if dloe['weight'] is not None:
            bg_level = 1 - (dloe['weight']**2).sum()/dloe['weight'].sum()
        else:
            bg_level = 0
        plt.plot(np.arange(0, 1, 1./args.length),
                 template*(1-bg_level)+bg_level, c='firebrick',
                 linewidth=2.0)
        plt.show()
        plt.close()
    except (TypeError, IOError):
        intf = itemplate.InteractiveFitter(
                dloe['phase'], nbins=args.lnbins,
                weights=dloe['weight'], skewed=args.skewed)
        intf.do_fit()
        print("Writing Gaussian-style template to %s ..." % args.template)
        intf.write_template(args.template)
        template = templatereader.read_gaussfitfile(args.template, args.length)
        template /= template.sum()*(1./args.length)  # normalize it
    # Since template is our pdf, no negative values allowed!
    # print(template)
    if any(template < 0):
        print(" ! ERROR: Your template contains negative values!")
        sys.exit()

    # If events were shifted in phase, shift them back and shift template
    if args.shift != 0:
        dloe['phase'] = np.where(dloe['phase'] < args.shift,
                                 dloe['phase']+(1-args.shift),
                                 dloe['phase']-args.shift)
        template = np.roll(template, int(args.length*(1-args.shift)))

    # Get VHE photons
    dhie = getData(
            args.eventfile, phsField=args.phasecol,
            mjdRange=[args.minMJD, args.maxMJD],
            eRange=[float(args.hie[0]), args.maxe])
    print("Extracted %i high energy photons" % dhie['energy'].size)
    # Make sure phases are between 0 and 1
    if any(dhie['phase']) < 0:
        print(" ! WARNING: I found negative phases!")
#    # Apply the desired shift for the fit
#    dhie['phase'] = np.where(dhie['phase'] > (1-args.shift),
#                             dhie['phase']-(1-args.shift),
#                             dhie['phase']+args.shift)

    # Discard photons outside of our extractions radius
    roi_ra, roi_dec = getRaDec(args.eventfile)
    print("roi: ", roi_ra, roi_dec)
    target = SkyCoord(roi_ra*u.degree, roi_dec*u.degree, frame='icrs')
    photoncoords = SkyCoord(dhie['ra']*u.degree, dhie['dec']*u.degree,
                            frame='icrs')
    idx = np.logical_or(
            np.logical_and(photoncoords.separation(target).deg < args.radfront,
                           dhie['conversion'] == 0),
            np.logical_and(photoncoords.separation(target).deg < args.radback,
                           dhie['conversion'] == 1))
    dhie['phase'] = dhie['phase'][idx]
    dhie['energy'] = dhie['energy'][idx]

    # Calc p-value and make plot
    dc = templatereader.get_dc_from_gausstemplate(args.template)
    fig = plt.figure(figsize=(4*1.1, 3*1.1))
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.set_xlabel("Phase")
    ax1.set_ylabel("Weighted low energy counts")
    ax1.hist(dloe['phase'], args.lnbins, range=(0., 1.),
             weights=dloe['weight'], histtype='step', color='black',
             linewidth=2, normed=True,
             label="above %.0f GeV" % (float(args.loe[0])/1000.))
    bg_level = 1 - (dloe['weight']**2).sum()/dloe['weight'].sum()
    ax1.plot(np.arange(0, 1, 1./args.length),
             template*(1-bg_level)+bg_level, color='k')
    ax1.yaxis.get_major_ticks()[0].label1.set_visible(False)
    # ax1.set_ylim(0.0, ax1.get_ylim()[1]*1.1)
    # ax1.legend(loc=2)

    ax2 = ax1.twinx()
    alphas = [0.7, 1.0, 1.0, 1.0]
    for i in range(len(args.hie)):
        idx = dhie['energy'] > float(args.hie[i])
        print("pulsed prob for >%.1f GeV:" % (float(args.hie[i])/1000.))
        pvalue = vhe_pulsed_prob(dhie['phase'][idx], template, dc)
        ax2.hist(dhie['phase'][idx], args.hnbins, range=(0., 1.),
                 color=args.cols[i], alpha=alphas[i], linewidth=0.5,
                 label="above %.0f GeV" % (float(args.hie[i])/1000.))
        print("Photons for >%.1f GeV:" % (float(args.hie[i])/1000.))
        print(dhie['phase'][idx])
        print(dhie['energy'][idx])
        if args.mc != 0:
            false_positive = 0
            if args.threshold != 0:
                pvalue = args.threshold
            print("mc test")
            size = dhie['phase'][idx].size
            for j in range(args.mc):
                p = vhe_pulsed_prob(np.random.rand(size), template, dc,
                                    verbose=False)
                if p < pvalue:
                    false_positive += 1
            pvalue = float(false_positive)/args.mc
            print("false positives: %.0f; rate is %.4f (%.2f s)" %
                  (false_positive, pvalue,
                   np.sqrt(stats.chi2.ppf(1-pvalue, 1))))

    ax2.set_ylim(bottom=0.0, top=ax2.get_ylim()[1]*2)

    ax2.get_yaxis().set_major_locator(MaxNLocator(integer=True))
    ax2.yaxis.get_major_ticks()[0].label2.set_visible(False)
    ax2.set_ylabel("High energy counts")
    # ax2.legend()

    lines1, labels1 = ax1.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    if not args.nolegend:
        ax2.legend(lines1+lines2, labels1+labels2, loc=2, frameon=False,
                   fontsize='small')
    ax1.text(float(args.postext.split()[0]), args.postext.split()[1],
             args.text, transform=ax1.transAxes, fontsize='medium')

    plt.tight_layout()
    plt.show()
