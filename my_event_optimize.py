#!/usr/bin/env python

import sys
import os
import math
import numpy as np
import templatereader
import pint.models
import pint.toa
from pint.utils import has_astropy_unit
from pint.eventstats import hmw, hm, h2sig, sf_hm
import pint.fermi_toas as fermi
from astropy.coordinates import SkyCoord
from pint.models.priors import Prior, UniformBoundedRV
import matplotlib.pyplot as plt
import scipy.optimize as op
import argparse
import corner
import emcee
import pickle
# import time


def get_lnprior_vals(model, errfact=1):
    """
    We multiply the error given in the par file by errfact
    """
    global modelin
    fitkeys = [p for p in modelin.params if not getattr(modelin, p).frozen]
    fitvals = []
    fiterrs = []
    for p in fitkeys:
        fitvals.append(getattr(modelin, p).value.value)
        fiterrs.append(getattr(modelin, p).uncertainty.value * errfact)
        # assume a 1% error if no error is given
        if fiterrs[-1] == 0:
            fiterrs[-1] = fitvals[-1]*0.01
    return fitkeys, np.asarray(fitvals), np.asarray(fiterrs)


def set_priors(fitkeys, fitvals, fiterrs):
    """
    By default use uniform priors on fit params
    of +-errfact * TEMPO errors
    """
    global modelin
    for key, v, e in zip(fitkeys, fitvals, fiterrs):
        if "GLTD_" in key:
            getattr(modelin, key).prior = Prior(UniformBoundedRV(1.0e-20, v+e))
            continue
        if "GLPH_" in key:
            getattr(modelin, key).prior = Prior(UniformBoundedRV(-0.5, 0.5))
            continue
        getattr(modelin, key).prior = Prior(UniformBoundedRV(v-e, v+e))


def profile_likelihood(phs, *otherargs):
    """
    A single likelihood calc for matching phases to a template.
    Likelihood is calculated as per eqn 2 in
    Pletsch & Clark 2015.
    """
    xvals, phases, template, weights = otherargs
    phss = phases.astype(np.float64) + phs
    # if phs < 0. or phs > 1.:
    #     return np.inf
    # ensure that all the phases are within 0-1
    phss[phss > 1.0] -= 1.0
    phss[phss < 0.0] += 1.0
    probs = np.interp(phss, xvals, template.astype(np.float64),
                      right=template[0])
    if weights is None:
        # logln = -np.log(probs).sum()
        return -np.log(probs).sum()
    else:
        # logln = -np.log(weights*probs + 1.0-weights).sum()
        return -np.log(weights*probs + 1.0-weights).sum()
    # print logln
    # return logln


def phaseogram(model=None, phs=None, shift=None, weights=None, bins=50,
               rotate=0.0, size=5, alpha=0.25, outFile=False):
    """
    Make a nice 2-panel phaseogram for the current model
    """
    global ts
    mjds = ts.table['tdbld'].astype(np.float64)
    if phs is None and model is None:
        print(" ERROR! You have to provide either a model or phases")
        sys.exit()
    if phs is None:
        phs = get_event_phases(model)
    if shift:
        shift = math.modf(shift)[0]
        phss = phs + shift
        phss[phss > 1.0] -= 1.0
        phss[phss < 0.0] += 1.0
    else:
        phss = get_event_phases(model)
    if outFile:
        print(" - saving", outFile)
    fermi.phaseogram(mjds, phss, weights=weights, bins=bins, rotate=rotate,
                     size=size, alpha=alpha, file=outFile)


def get_event_phases(model):
    """
    Return pulse phases based on the current model
    """
    global ts
    phss = model.phase(ts.table)[1]
    return np.where(phss < 0.0, phss + 1.0, phss)  # ensure all positive


def marginalize_over_phase(
        phases, template, weights=None, resolution=1.0/1024, minimize=True,
        fftfit=False, showplot=False, lophs=0.0, hiphs=1.0):
    """
    def marginalize_over_phase(
            phases, template, weights=None, resolution=1.0/1024, minimize=True,
            fftfit=False, showplot=False, lophs=0.0, hiphs=1.0):
    a pulse profile comprised of combined photon phases.  A maximum likelood
    technique is used.  The shift and the max log likehood are returned. You
    probably want to use "minimize" rathre than "fftfit" unless you are only
    sampling very close to your known min (fftfit not implemented here).
    """
    ltemp = len(template)
    xtemp = np.arange(ltemp) * 1.0/ltemp
    if minimize:
        phs, like = marginalize_over_phase(phases, template, weights,
                                           resolution=1.0/64, minimize=False,
                                           showplot=showplot)
        hwidth = 0.03
        lophs, hiphs = phs - hwidth, phs + hwidth
        result = op.minimize(profile_likelihood, [phs],
                             args=(xtemp, phases, template, weights),
                             bounds=[[lophs, hiphs]], method='L-BFGS-B')
        return result['x'], -result['fun']
    dphss = np.arange(lophs, hiphs, resolution)
    trials = phases.astype(np.float64) + dphss[:, np.newaxis]
    trials[trials > 1.0] -= 1.0  # ensure that all the phases are within 0-1
    probs = np.interp(trials, xtemp, template.astype(np.float64),
                      right=template[0])
    if weights is None:
        lnlikes = (np.log(probs)).sum(axis=1)
    else:
        lnlikes = (np.log(weights*probs + 1.0 - weights)).sum(axis=1)
    if showplot:
        plt.plot(dphss, lnlikes)
        plt.xlabel("Pulse Phase")
        plt.ylabel("Log likelihood")
        plt.show()
    return dphss[lnlikes.argmax()], lnlikes.max()


def getTOAs(eventfile, weights, ephem='DE405', planets=False, minw=0.5,
            minMJD=0., maxMJD=np.inf, minE=0., maxE=300000., usePickle=False):
    global ts
    # Set the target coords for automatic weighting if necessary
    target = SkyCoord(modelin.RAJ.value, modelin.DECJ.value, frame='icrs') \
        if weights == 'CALC' else None
    # TODO: make this properly handle long double
    if not (os.path.isfile(eventfile+".pickle") or
            os.path.isfile(eventfile+".pickle.gz")) or not usePickle:
        # Read event file and return list of TOA objects
        tl = fermi.load_Fermi_TOAs(eventfile, weightcolumn=weights,
                                   targetcoord=target, minweight=minw,
                                   minmjd=minMJD, maxmjd=maxMJD,
                                   minE=minE, maxE=maxE)
        print("There are %d events we will use" % len(tl))
        # Now convert to TOAs object and compute TDBs and posvels
        ts = pint.toa.TOAs(toalist=tl)
        ts.filename = eventfile
        # ts.apply_clock_corrections()
        ts.compute_TDBs()
        ts.compute_posvels(ephem=ephem, planets=planets)
        # ts.compute_posvels(ephem="DE405", planets=True)
        ts.pickle()
    else:  # read the events in as a picke file
        ts = pint.toa.TOAs(eventfile, usepickle=True)


def set_params(model, fitp):
    """Set the model parameters to the value contained in the input dict.
    Ex. fitter.set_params({'F0':60.1,'F1':-1.3e-15})
    """
    for k, v in fitp.items():
        # The check for astropy units should be
        # able to go away once params are fixed
        getattr(model, k).num_value = v.value \
                                           if has_astropy_unit(v) else v


def set_uncert(model, fitp):
    """
    Set the model uncertainties to the value contained in the input dict.
    """
    for k, v in fitp.items():
        # The check for astropy units should be
        # able to go away once params are fixed
        getattr(model, k).num_uncertainty = v.value \
                                            if has_astropy_unit(v) else v


def lnPosterior(theta, fitvals, fiterrs, fitkeys, template, weights):
    """
    The log posterior (priors * likelihood)
    """
    global maxPost, ncalls, modelin, maxTheta, onepercent, ronepercent
    ncalls += 1
    if ncalls % ronepercent == 0:
        print("~%d%% complete" % (ncalls / onepercent))
        # global time0
        # print time.time() - time0
        # time0 = time.time()

    set_params(modelin, dict(zip(fitkeys, theta)))
    phases = get_event_phases(modelin)
    # lnpost = lnprior + lnlikelihood
    lnpost = lnPrior(theta, fitvals, fiterrs, fitkeys) + \
        marginalize_over_phase(phases, template, weights=weights)[1]

    if lnpost > maxPost:
        print("New max: ", lnpost)
        for name, val in zip(fitkeys, theta):
                print("  %8s: %25.25g" % (name, val))
        maxPost = lnpost
        maxTheta = theta
    return lnpost


def lnPosterior_htest(theta, fitvals, fiterrs, fitkeys):
    """
    The log posterior (priors * likelihood)
    """
    global maxPost, ncalls, modelin, maxTheta, onepercent, ronepercent
    ncalls += 1
    if ncalls % ronepercent == 0:
        print("~%d%% complete" % (ncalls / onepercent))
        # global time0
        # print time.time() - time0
        # time0 = time.time()

    set_params(modelin, dict(zip(fitkeys, theta)))
    phases = get_event_phases(modelin)
    # Here, I need to negate the survival function of H, so I am looking
    # for the maximum
    lnlikelihood = -1.0*sf_hm(hm(phases), logprob=True)
    lnpost = lnPrior(theta, fitvals, fiterrs, fitkeys) + lnlikelihood
    if lnpost > maxPost:
        print("New max: ", lnpost)
        for name, val in zip(fitkeys, theta):
                print("  %8s: %25.25g" % (name, val))
        maxPost = lnpost
        maxTheta = theta
    return lnpost


def lnPosterior_whtest(theta, fitvals, fiterrs, fitkeys, weights):
    """
    The log posterior (priors * likelihood)
    """
    global maxPost, ncalls, modelin, maxTheta, onepercent, ronepercent
    set_params(modelin, dict(zip(fitkeys, theta)))
    phases = get_event_phases(modelin)
    # Here, I need to negate the survival function of H, so I am looking
    # for the maximum
    lnlikelihood = -1.0*sf_hm(hmw(phases, weights=weights), logprob=True)
    ncalls += 1
    if ncalls % ronepercent == 0:
        print("~%d%% complete" % (ncalls / onepercent))
        # global time0
        # print time.time() - time0
        # time0 = time.time()
    lnpost = lnPrior(theta, fitvals, fiterrs, fitkeys) + lnlikelihood
    if lnpost > maxPost:
        print("New max: ", lnpost)
        for name, val in zip(fitkeys, theta):
                print("  %8s: %25.25g" % (name, val))
        maxPost = lnpost
        maxTheta = theta
    return lnpost


def lnPrior(theta, fitvals, fiterrs, fitkeys):
    """
    The log prior
    """
    global modelin
    lnsum = 0.0
    for val, mn, sig, key in zip(theta, fitvals, fiterrs, fitkeys):
        lnsum += getattr(modelin, key).prior_pdf(val, logpdf=True)
        # lnsum += (-np.log(np.sqrt(2.0 * np.pi) * sig) -
        #          0.5 * ((val - mn) / sig)**2.0)
    return lnsum


def readWeights(weightcol):
    global ts
    if weightcol is not None:
        weights = np.asarray([x['weight'] for x in ts.table['flags']])
        wmx, wmn = weights.max(), weights.min()
        # make the highest weight = 1, but keep min weight the same
        # weights = wmn + ((weights - wmn) * (1.0 - wmn) / (wmx - wmn))
        print("There are %d events, with min / max weights %.3f / %.3f" %
              (len(weights), wmn, wmx))
        return weights
    weights = None
    print("There are %d events, no weights are being used." %
          (len(ts.table)))
    return weights


def chains_to_dict(names, sampler, burnin=0):
    if burnin > 0:
        chains = [sampler.chain[:, burnin:, ii].T for ii in range(len(names))]
    else:
        chains = [sampler.chain[:, :, ii].T for ii in range(len(names))]
    return dict(zip(names, chains))


def plot_chains(chain_dict, outFile=False):
    np = len(chain_dict)
    fig, axes = plt.subplots(np, 1, sharex=True, figsize=(8, 9))
    for ii, name in enumerate(chain_dict.keys()):
        axes[ii].plot(chain_dict[name], color="k", alpha=0.3)
        axes[ii].set_ylabel(name)
    axes[np-1].set_xlabel("Step Number")
    fig.tight_layout()
    if outFile:
        print(" - saving chain plot: "+outFile)
        fig.savefig(outFile)
        plt.close()
    else:
        plt.show()


def main():
    # Parse command line arguments
    desc = "Use PINT and emcee to estimate parameters for a timing model " + \
           "by either maximizing the htest or fitting it to a pulse template."
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("eventfile", help="Fermi event FITS file name. " +
                        "Should be GEOCENTERED.")
    parser.add_argument("parfile", help="Par file to construct model from.")
    parser.add_argument("--template", help="Pulse template file. For now " +
                        "only gaussian templates are supported.", default=None)
    parser.add_argument("--outd", help="Directory for the output " +
                        "(default ./).", default="./")
    parser.add_argument("--weightcol", help="Column name for event weights " +
                        "(or 'CALC' to compute them)", default=None)
    parser.add_argument("--minweight", help="Minimum weight (default: -1)",
                        default=-1, type=float)
    parser.add_argument("--maxMJD", help="Maximum MJD to include in analysis",
                        default=np.inf, type=float)
    parser.add_argument("--minMJD", help="Minimum MJD to include in analysis",
                        default=0., type=float)
    parser.add_argument("--maxE", help="Maximum energy to include in analysis",
                        default=300000., type=float)
    parser.add_argument("--minE", help="Minimum energy to include in analysis",
                        default=0., type=float)
    parser.add_argument("--planets", help="Use planetary Shapiro delay in " +
                        "calculations (default False)", default=False,
                        action="store_true")
    parser.add_argument("--ephem", help="Planetary ephemeris to use " +
                        "(default DE405)", default="DE405")
    parser.add_argument("--nwalkers", help="Number of walkers in emcee " +
                        "(default 200).", default=200, type=int)
    parser.add_argument("--nsteps", help="Number of steps for each walker " +
                        "(default 200).", default=200, type=int)
    parser.add_argument("--burnin", help="Number of initial steps to be " +
                        "burned (default 400).", default=400, type=int)
    parser.add_argument("--nthreads", help="Number of threads to be spawn.",
                        default=1, type=int)
    parser.add_argument("--errfact", help="The starting points of the " +
                        "walkers will be inside the given errors in the par " +
                        "file multiplied by errfact (default 10).", default=10,
                        type=float)
    parser.add_argument("--ninterpol", help="Number of interpolations in the" +
                        "pulse template (default 1000).",
                        default=1000, type=int)
    parser.add_argument("--pickle", help="Use pickled file if available " +
                        "(default False).", action="store_true", default=False)
    parser.add_argument("--spread", help="Spread out inicial state of " +
                        "walkers.", action="store_true", default=False)
    args = parser.parse_args()

    # -- Read in initial model -- #
    ###############################
    global modelin
    modelin = pint.models.get_model(args.parfile)
    # Remove the dispersion delay as it is unnecessary
    modelin.delay_funcs['L1'].remove(modelin.dispersion_delay)
    fitkeys, fitvals, fiterrs = get_lnprior_vals(modelin, errfact=args.errfact)
    set_priors(fitkeys, fitvals, fiterrs)
    print(fitkeys, fitvals, fiterrs)

    # --- Read in "TOAs" and get initial phases --- #
    #################################################
    global ts
    getTOAs(args.eventfile, args.weightcol, ephem=args.ephem,
            planets=args.planets, minw=args.minweight,
            minMJD=args.minMJD, maxMJD=args.maxMJD,
            minE=args.minE, maxE=args.maxE,
            usePickle=args.pickle)
    phases = get_event_phases(modelin)

    # --- Read in weights ---#
    ##########################
    weights = readWeights(args.weightcol)
    # if weights is not None and args.minweight == 0:
    #     prof_vs_weights(phases, weights)
    #     prof_vs_weights(phases, weights, use_weights=True)
    #     sys.exit()

    # --- Load the template --- #
    #############################
    if args.template is not None:
        gtemplate = templatereader.read_gaussfitfile(args.template,
                                                     args.ninterpol)
        gtemplate /= gtemplate.sum()*(1./args.ninterpol)  # normalize it
        if any(gtemplate < 0):
            print(" ! ERROR: You have negative values in your template!")
            sys.exit()

    # --- Plot starting phaseogram --- #
    ####################################
    if args.template is not None:
        shift0, ln0 = marginalize_over_phase(phases, gtemplate,
                                             weights=weights)
        print(" - pre shift:", shift0)
        print(" - pre pulse likelihood:", ln0)
        phaseogram(modelin, phs=phases, shift=shift0, weights=weights,
                   outFile=args.outd+"/"+modelin.PSR.value+"_pre.png")
    else:
        lnlikeli0 = -1.0*sf_hm(hmw(phases, weights=weights), logprob=True) \
            if weights is not None else -1.0*sf_hm(hm(phases), logprob=True)
        print(" - pre pulse likelihood:", lnlikeli0)
        phaseogram(modelin, phs=phases, weights=weights,
                   outFile=args.outd+"/"+modelin.PSR.value+"_pre.png")

    # --- Starting values for MCMC --- #
    ####################################
    ndim = len(fitkeys)
    if args.spread:
        pos = [fitvals + fiterrs *
               np.random.uniform(-1., 1., ndim) for i in range(args.nwalkers)]
    else:
        pos = [fitvals + (fiterrs/args.errfact) *
               np.random.uniform(-1., 1., ndim) for i in range(args.nwalkers)]
    # Set the 0th walker to have the initial pre-fit solution
    # This way, one walker should always be in a good position
    pos[0] = fitvals

    # --- MCMC --- #
    ################
    global ncalls, maxPost, time0, maxTheta, onepercent, ronepercent
    maxPost = -np.inf
    ncalls, time0 = 0, 0
    onepercent = (args.nwalkers * (args.burnin+args.nsteps+1) /
                  (100.*args.nthreads))
    ronepercent = int(np.ceil(onepercent)+0.5)
    if args.template is not None:
        sampler = emcee.EnsembleSampler(
                        args.nwalkers, ndim, lnPosterior,
                        args=[fitvals, fiterrs, fitkeys, gtemplate, weights],
                        threads=args.nthreads)
    elif weights is not None:
        sampler = emcee.EnsembleSampler(
                        args.nwalkers, ndim, lnPosterior_whtest,
                        args=[fitvals, fiterrs, fitkeys, weights],
                        threads=args.nthreads)
    else:
        sampler = emcee.EnsembleSampler(
                        args.nwalkers, ndim, lnPosterior_htest,
                        args=[fitvals, fiterrs, fitkeys],
                        threads=args.nthreads)

    # first the burn-in
    pos0, prob, state = sampler.run_mcmc(pos, args.burnin)
    chains = chains_to_dict(fitkeys, sampler)
    plot_chains(chains, outFile=args.outd+"/"+modelin.PSR.value +
                "_chains_burnin.png")
    # sample steps
    sampler.reset()
    sampler.run_mcmc(pos0, args.nsteps, rstate0=state)
    # print out some stats
    print("acceptance fraction:", sampler.acceptance_fraction)
    frac = sampler.acceptance_fraction[
                sampler.acceptance_fraction > 0.2]
    print("faction between 0.2 and 0.5:",
          frac[frac < 0.5].size / float(sampler.acceptance_fraction.size))
    try:
        print("autocorrelation time: ", sampler.acor)
    except emcee.autocorr.AutocorrError:
        print("Could not determine autocorrelation time, probably the chain is"
              " too short!")

    # --- Plot chains and triangles --- #
    #####################################
    samples = sampler.chain[:, :, :].reshape((-1, ndim))
    pickle.dump(samples, open(args.outd+"/"+modelin.PSR.value +
                              '_samples.pickle', 'wb'))
    chains = chains_to_dict(fitkeys, sampler)
    plot_chains(chains, outFile=args.outd+"/"+modelin.PSR.value +
                "_chains.png")

    samples = sampler.chain[:, :, :].reshape((-1, ndim))
    fig = corner.corner(samples, labels=fitkeys, bins=50)
    trianglePlot = args.outd+"/"+modelin.PSR.value+"_triangle.png"
    print(" - saving triangle plot: "+trianglePlot)
    fig.savefig(trianglePlot)

    # --- Plot phaseogram with the 50th percentile values --- #
    ###########################################################
    ranges = map(lambda v: (v[1], v[2]-v[1], v[1]-v[0]),
                 zip(*np.percentile(samples, [16, 50, 84], axis=0)))
    set_params(modelin, dict(zip(fitkeys, np.percentile(samples, 50, axis=0))))
    uncertainties = [(float(v[1])+float(v[2]))*0.5 for v in ranges]
    print(uncertainties)
    set_uncert(modelin, dict(zip(fitkeys, uncertainties)))

    phases = get_event_phases(modelin)
    if args.template is not None:
        shift1, ln1 = marginalize_over_phase(phases, gtemplate,
                                             weights=weights)
        print(" - post shift:", shift1)
        print(" - post pulse likelihood:", ln1)
        phaseogram(modelin, phs=phases, shift=shift1, weights=weights,
                   outFile=args.outd+"/"+modelin.PSR.value+"_post.png")
    else:
        lnlikeli1 = -1.0*sf_hm(hmw(phases, weights=weights), logprob=True) \
            if weights is not None else -1.0*sf_hm(hm(phases), logprob=True)
        print(" - post pulse likelihood:", lnlikeli1)
        phaseogram(modelin, phs=phases, weights=weights,
                   outFile=args.outd+"/"+modelin.PSR.value+"_post.png")
    if weights is not None:
        hval = np.float64(hmw(phases, weights=weights))
    else:
        hval = np.float64(hm(phases))
    print("Htest : {0:.2f} ({1:.2f} sig)".format(hval, h2sig(hval)))

    # --- Print and save the best MCMC values and ranges --- #
    ##########################################################
    print("Post-MCMC values (50th percentile +/- (16th/84th percentile):")
    for name, vals in zip(fitkeys, ranges):
        print("%8s:" % name, "%25.25g (+ %25.25g  / - %25.25g)" % vals,
              "+- %25.25g" % np.float128((vals[1]+vals[2])/2.))

    with open(args.outd+"/"+modelin.PSR.value+"_results.txt", 'w') as f:
        f.write("Post-MCMC values " +
                "(50th percentile +/- (16th/84th percentile):\n")
        for name, vals in zip(fitkeys, ranges):
            f.write("%8s:" % name +
                    " %25.15g (+ %25.15g  / - %25.15g)" % vals +
                    " +- %25.15g \n" % np.float128((vals[1]+vals[2])/2.))
        f.write("\nMaximum likelihood par file:\n")
        f.write(modelin.as_parfile())

    # --- Plot phaseogram with the "maximum" values --- #
    #####################################################
    if args.nthreads == 1:
        print(" - maximum values:")
        print(dict(zip(fitkeys, maxTheta)))
        set_params(modelin, dict(zip(fitkeys, maxTheta)))
        phases = get_event_phases(modelin)
        if args.template is not None:
            shift1, ln1 = marginalize_over_phase(phases, gtemplate,
                                                 weights=weights)
            print(" - post shift:", shift1)
            print(" - post pulse likelihood:", ln1)
            phaseogram(modelin, phs=phases, shift=shift1, weights=weights,
                       outFile=args.outd+"/"+modelin.PSR.value+"_best.png")
        else:
            lnlikeli1 = \
                    -1.0*sf_hm(hmw(phases, weights=weights), logprob=True) \
                    if weights is not None else -1.0*sf_hm(hm(phases),
                                                           logprob=True)
            print(" - post pulse likelihood:", lnlikeli1)
            phaseogram(modelin, phs=phases, weights=weights,
                       outFile=args.outd+"/"+modelin.PSR.value+"_best.png")
        if weights is not None:
            hval = np.float64(hmw(phases, weights=weights))
        else:
            hval = np.float64(hm(phases))
        print("Htest : {0:.2f} ({1:.2f} sigma)".format(hval, h2sig(hval)))

    sys.exit()


if __name__ == "__main__":
    main()
