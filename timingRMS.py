#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import itemplate
import templatereader
from optparse import OptionParser
from pint.eventstats import hmw, hm, h2sig
import pint.fermi_toas as fermi
from pulsartiming import marginalize_over_phase, prof_vs_weights
import sys


def createTestSample():  # just a test sample
    num = 2500
    phsslist = []
    for i in range(num):
        if np.random.rand() < 0.7:
            if np.random.rand() < 0.75:
                phs = np.random.normal(0.2, 0.02)
            else:
                phs = np.random.normal(0.7, 0.04)
            phs -= 1 if phs > 1 else 0
            phs += 1 if phs < 0 else 0
            phsslist.append(phs)
        else:
            phsslist.append(np.random.rand())
    phss = np.asarray(phsslist, dtype=float)
    mjdss = np.sort(np.random.rand(num) * 1000)
    weights = np.random.rand(num)
    return phss, mjdss, weights


def readParFile(parFile):
    tepoch, f0, f1, f2 = 0, 0, 0, 0
    with open(parFile, 'r') as f:
        for line in f:
            if line.lstrip().startswith("PEPOCH"):
                tepoch = np.float128(line.split()[1])
            if line.lstrip().startswith("F0"):
                f0 = np.float128(line.split()[1])
            if line.lstrip().startswith("F1"):
                f1 = np.float128(line.split()[1])
            if line.lstrip().startswith("F2"):
                f2 = np.float128(line.split()[1])
    return tepoch, f0, f1, f2


def main():
    # Should change to argparse someday! As of py2.7, optparse is depracted
    desc = "Read an FT1 file containing PULSE_PHASE info and plot the " + \
           "timing noise by fitting (unbinned likelihood) a template to " + \
           "the total and binned (in time) phaseograms."
    parser = OptionParser(usage=" %prog [options] [FT1_FILENAME]",
                          description=desc)
    parser.add_option('-n', '--nbins', type='int', default=50,
                      help="Number of bins to use in phase histogram.")
    parser.add_option('-c', '--weightcol', type='string', default=None,
                      help='Column in FT1 file that holds the weight')
    parser.add_option('-m', '--min_weight', type='float', default=-1,
                      help='Minimum weight to include in fit.')
    parser.add_option('-x', '--maxMJD', type='float', default=-1,
                      help='Maximum MJD to include in fit.')
    parser.add_option('-y', '--minMJD', type='float', default=-1,
                      help='Minimum MJD to include in fit.')
    parser.add_option('-t', '--template', type='string',
                      default='template.txt', help='Provide a template or ' +
                      'give the output name of the template file (default: ' +
                      'template.txt).')
    parser.add_option('-l', '--length', type='int', default=10000,
                      help='Interpolated points for template (default 10000)')
    parser.add_option('-s', '--minSig', type='float', help='Minimum ' +
                      'significance required in a time bin (default 15[sig])',
                      default=15)
    parser.add_option('-r', '--rate', type='float', default=1,
                      help='Every r (in MJD) we evaluate the significance')
    parser.add_option('-p', '--par', type='string', default=None,
                      help='Par file in tempo format to convert phase ' +
                      'differences in time differences.')
    parser.add_option('--test', action='store_true', default=False,
                      help="Create a test sample instead of using data")
    parser.add_option('--prof', action='store_true', default=False,
                      help="Plot profiles vs minimum weight cut and exit.")
    parser.add_option('--shift',  type=float, default=0.0,
                      help="Shift phases if problems with the fitter.")
    parser.add_option('-o', '--overallshift',  action='store_true',
                      default=False,
                      help="Ignore overall shift!")
    parser.add_option('--skewed',  action='store_true', default=False,
                      help="Do you want to fit skewed Gaussians?")

    # Parse arguments
    (opts, args) = parser.parse_args()
    if not opts.test and len(args) < 1:
        raise ValueError('Must provide an FT1 file!')
    useweights = True if opts.weightcol is not None else False

    # Get phases, weights and mjds out of the FT1 file
    if opts.test:
        phases, mjds, weights = createTestSample()
    else:
        phases, weights, mjds = \
                itemplate.get_phases(args[0], get_weights=useweights,
                                     weightcol=opts.weightcol)
    if not useweights:
        weights = np.ones(phases.size)
    # To make sure phases are between 0 and 1
    if any(phases) < 0:
        print(" ! WARNING: I found phases < 0!")
    phases = np.where(phases < 0, phases + 1, phases)
    # Apply the desired shift
    phases = np.where(phases > (1-opts.shift), phases-(1-opts.shift),
                      phases+opts.shift)

    # Perform maxMJD and minWeight cut
    if opts.maxMJD > 0:
        phases = phases[mjds < opts.maxMJD]
        print('%d of %d photons survive maxMJD cut' %
              (len(phases), len(mjds)))
        weights = weights[mjds < opts.maxMJD]
        mjds = mjds[mjds < opts.maxMJD]
    if opts.minMJD > 0:
        phases = phases[mjds > opts.minMJD]
        print('%d of %d photons survive minMJD cut' %
              (len(phases), len(mjds)))
        weights = weights[mjds > opts.minMJD]
        mjds = mjds[mjds > opts.minMJD]
    if useweights:
        phases = phases[weights > opts.min_weight]
        print('%d of %d photons survive weight cut' %
              (len(phases), len(weights)))
        mjds = mjds[weights > opts.min_weight]
        weights = weights[weights > opts.min_weight]
    print("First MJD: %.0f | Last MJD: %.0f" % (mjds[0], mjds[-1]))

    # Plot min weight cut vs significance
    if opts.prof:
        prof_vs_weights(phases, weights, nbins=opts.nbins)
        #                minWeights=[0., 0.02, 0.04, 0.06, 0.08, 0.1])
        sys.exit()

    # Get template from file or do a fit and write it to a file
    try:
        template = templatereader.read_gaussfitfile(opts.template, opts.length)
        print("Got template from %s ..." % opts.template)
        template /= template.sum()*(1./opts.length)  # normalize it
        # copied from itemplate...
        itemplate.light_curve(phases, weights=weights, nbins=opts.nbins)
        bg_level = 1 - (weights**2).sum()/weights.sum()
        plt.plot(np.arange(0, 1, 1./opts.length),
                 template*(1-bg_level)+bg_level, c='firebrick',
                 linewidth=2.0)
        plt.show()
        plt.close()
        # Get overall shift of the template, actually just necessary if
        # template was not obtained with the exact same data set ...
        dphall, logmax = marginalize_over_phase(phases, template, error=False,
                                                weights=weights)
        print('Overall shift of used template: %f' % dphall)
    except (TypeError, IOError):
        intf = itemplate.InteractiveFitter(phases, nbins=opts.nbins,
                                           weights=weights,
                                           skewed=opts.skewed)
        intf.do_fit()
        dphall = 0.
        print('Writing Gaussian-style template to %s ...' % opts.template)
        intf.write_template(opts.template)
        template = templatereader.read_gaussfitfile(opts.template, opts.length)
        template /= template.sum()*(1./opts.length)  # normalize it
    # Since template is our pdf, no negative values allowed!
    if any(template < 0):
        print(" ! ERROR: Your template contains negative values!")
        sys.exit()

    # If events were shifted in phase, shift them back and shift template
    if opts.shift != 0:
        phases = np.where(phases < opts.shift, phases+(1-opts.shift),
                          phases-opts.shift)
        template = np.roll(template, int(opts.length*(1-opts.shift)))

    # Get phase differences from fits to phaseograms in time bins
    dphs, mjds0, logmaxs, errs, sigs = [], [], [], [], []
    i0, i, ii = 0, 0, 0
    while i < len(mjds):
        sig = 0
        # perform fits to phaseograms with the ~same significance
        if opts.minSig != 0:
            while sig < opts.minSig:
                try:
                    while mjds[i]-mjds[ii] < opts.rate:
                        i += 1
                # when reaching the end of our data
                except IndexError:
                    break
                ii = i
                # calculate significance
                if useweights:
                    hval = hmw(phases[i0:i], weights=weights[i0:i])
                else:
                    hval = hm(phases[i0:i])
                sig = h2sig(hval)
        # discard last point if not significant
            if sig < opts.minSig:
                break
        else:
            mjd0 = mjds[i]
            try:
                while mjds[i] - mjd0 < opts.rate:
                    i += 1
                # calculate significance
                if useweights:
                    hval = hmw(phases[i0:i], weights=weights[i0:i])
                else:
                    hval = hm(phases[i0:i])
                sig = h2sig(hval)
            except IndexError:
                break
        # fit the template to the phaseogram
        if useweights:
            dph, logmax, err = \
                marginalize_over_phase(phases[i0:i], template, error=True,
                                       weights=weights[i0:i], showplot=False)
        else:
            dph, logmax, err = \
                marginalize_over_phase(phases[i0:i], template, error=True,
                                       showplot=False)
        # save the phase differenes,the max log Likelihood and the errors
        if (opts.overallshift):
            dphall = 0.
        print("mjd: %.0f; dphs: %f +- %f; sig: %.1f" %
              (mjds[i0], dph-dphall, err, sig))
        dphs.append(dph-dphall)
        logmaxs.append(logmax)
        errs.append(err)
        mjds0.append(mjds[i0])
        sigs.append(sig)
        i0 = i
    errsar = np.asarray(errs, dtype='float')
    dphsar = np.asarray(dphs, dtype='float')
    dphsar[dphsar > 0.5] -= 1.  # ensure that values are within -0.5 and 0.5
    mjds0ar = np.asarray(mjds0, dtype='float')

    # Read par file to convert phase differences into time differences
    transf = 1  # for plotting the TOA residuals in time units
    if opts.par is not None:
        try:
            tepoch, f0, f1, f2 = readParFile(opts.par)
            if f0 == 0:
                raise(IOError)
            print("tepoch: %f; f0: %f; f1: %f; f2: %f" % (tepoch, f0, f1, f2))
            transf = 1./f0
            # transf = 1./(f0 + f1*(mjds0ar-tepoch)*86400 +
            #              (f2/2.)*(mjds0ar-tepoch)**2*86400**2)
            # dphsar = dphsar * transf
            # errsar = errsar * transf
        except IOError:
            print(" ! WARNING: Cannot read given par file!")
            opts.par = None

    fermi.phaseogram(mjds, phases, weights=weights, bins=opts.nbins)
    with open('./dTOAs_minW_%.2f_minS_%.0f_temp_%s.txt' %
              (opts.min_weight, opts.minSig,
               opts.template.replace('.txt', '')), 'w') as f:
        f.write('mjd dph[mP] dpherr[mP] sig\n')
        for i in range(mjds0ar.size):
            f.write('%.2f %.2f %.2f %.2f\n' %
                    (mjds0ar[i], 1000*dphsar[i], 1000*errsar[i], sigs[i]))
    fig, ax1 = plt.subplots()
    ax1.errorbar(mjds0ar, dphsar*1000, yerr=errsar*1000, ls='None', marker='o')
    ax1.axhline(c='black')
    ax1.set_xlabel('MJD')
    ax1.set_ylabel('TOA residual [mPeriod]')

    print("RMS [Period]: %5.15g" % np.sqrt(np.mean(np.square(dphsar))))

    if opts.par is not None:
        ax2 = ax1.twinx()
        ax2.set_ylim(ax1.get_ylim()[0]*transf, ax1.get_ylim()[1]*transf)
        # label_text = ["%.1f" % float(loc*10**3) for loc in ax2.get_yticks()]
        # ax2.set_yticklabels(label_text)
        ax2.set_ylabel('TOA residual [ms]')
        print("RMS [s]: %5.15g" % np.sqrt(np.mean(np.square(dphsar*transf))))

    # Edit the text to your liking
    # label_text = ["%.1f" % float(loc*10**3) for loc in ax1.get_yticks()]
    # ax1.set_yticklabels(label_text)

    # years = (mjds - 51544.0) / 365.25 + 2000.0
    # ax3 = ax1.twiny()
    # print (ax1.get_xlim()[0] - 51544.0)/365.25 + 2000.0
    # ax3.set_xlim((ax1.get_xlim()[0] - 51544.0)/365.25 + 2000.0,
    #              (ax1.get_xlim()[1] - 51544.0)/365.25 + 2000.0)
    # ax3.set_xlabel('Year')

    plt.show()


if __name__ == '__main__':
    main()
