#!/bin/bash

if [ $# -ne 4 ]; then
    echo "Usage: $0 evtfile outfile ra dec"
    exit 1
fi

evfile=$1
outfile=$2
ra=$3
dec=$4
scfile=/media/david/Data/Fermi/enrico/Data/download/lat_spacecraft_merged.fits

gtbary $evfile $scfile $outfile $ra $dec tcorrect=GEO
