evtfile=$1
parfile=$2

wc="J0102+4839"
temp="./temp_x_55869_m_0.1.txt"
mw=0.1
nw=200
ns=100
nb=100
nt=3
ef=1000

my_event_optimize --template=$temp --weightcol=$wc --minweight=$mw --planets \
    --nwalkers=$nw --nsteps=$ns --burnin=$nb --nthreads=$nt --errfact=$ef \
    $evtfile $parfile
