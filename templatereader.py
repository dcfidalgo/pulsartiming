#!/usr/bin/env python
"""
Provide a method for reading the itemplate output
in case of gaussian templates.

"""
from uw.pulsar.lcprimitives import LCGaussian
import numpy as np
import math


def get_dc_from_gausstemplate(gaussfitfile):
    """
    Get dc (constant) component of the gauss template.
    """
    with open(gaussfitfile) as f:
        for line in f:
            if line.lstrip().startswith("const"):
                return float(line.split()[2])
    return None


def read_gaussfitfile(gaussfitfile, proflen=1000, nr=0):
    """
    Read a Gaussian-fit file as created by pygaussfit.py (itemplate.py).

    Parameters
    ----------
    gaussfitfile : string
       Gaussian-fit file
    proflen : int, optional, default=1000
       Number of points in the profile.
    nr : int, optional, default=0
       Get only the nr Gaussian. By default get all (nr=0).

    Return
    ------
    A numpy array of length 'proflen' is returned.
    """
    # for now we support up to 20 primitives
    nprim = 20
    components = ['phas', 'ampl', 'fwhm', 'widthl', 'widthr']
    primitive = dict(zip(components, [0]*len(components)))
    prims = [primitive.copy() for n in range(nprim)]
    const = []

    with open(gaussfitfile) as f:
        for l in f:
            key = l.lstrip().split()[0]
            if key.startswith("const"):
                const.append(float(l.split()[2]))
                continue
            for c in components:
                if key.startswith(c):
                    prims[int(key[-1])-1][c] = float(l.split()[2])

    template = np.zeros(proflen, dtype='float64')
    for ii in range(len(const)):
        template += const[ii]
    for ii in range(nprim):
        if prims[ii]['ampl'] == 0:
            continue
        if prims[ii]['fwhm'] == 0:
            template += prims[ii]['ampl'] * \
                    gaussian_profile(proflen, prims[ii]['phas'],
                                     prims[ii]['widthl'], prims[ii]['widthr'])
        else:
            template += prims[ii]['ampl'] * \
                    gaussian_profile(proflen, prims[ii]['phas'],
                                     prims[ii]['fwhm']/2.35482,
                                     prims[ii]['fwhm']/2.35482)

    return template


def gaussian_profile(N, phase, widthl, widthr):
    """
    gaussian_profile(N, phase, widthl, widthr):
        Return a gaussian pulse profile with 'N' bins and
        an integrated 'flux' of 1 unit.
            'N' = the number of points in the profile
            'phase' = the pulse phase (0-1)
            'widthl, widthr' = the gaussian sigmas
        Note:  The FWHM of a gaussian is approx 2.35482 sigma
    """
    mean = phase % 1.0
    phsval = np.arange(2*N, dtype='d') / float(N)
    if (mean < 0.5):
        phsval = np.where(np.greater(phsval, mean+1.0),
                          phsval-2.0, phsval)
    else:
        phsval = np.where(np.less(phsval, mean-1.0),
                          phsval+2.0, phsval)
    try:
        zs = (phsval-mean)/widthl
        idx = np.where(phsval-mean > 0)
        zs[idx] = (phsval[idx]-mean)/widthr
        okzinds = np.compress(np.fabs(zs) < 20.0, np.arange(2*N))
        okzs = np.take(zs, okzinds)
        retval = np.zeros(2*N, 'd')
        np.put(retval, okzinds,
               np.exp(-0.5*(okzs)**2.0) *
               2./(np.sqrt(2*math.pi)*(widthl+widthr)))
        return retval[:N]+retval[N:]
    except OverflowError:
        print("Problem in gaussian prof:  mean = %f  sigmal = %f  sigmar = " %
              (mean, widthl, widthr))
        return np.zeros(N, 'd')


def read_gaussTemplate(inFile):
    """
    read_gaussTemplate(inFile):
    Read a Gaussian-fit file as created by the output of pygaussfit.py
    or itemplate.py.
    The input parameter is the name of the file. A list of LCGaussian
    objects is returned.
    """
    const = []
    phass = []
    ampls = []
    fwhms = []
    primitives = []
    for line in open(inFile):
        if line.lstrip().startswith("const"):
            const.append(float(line.split()[2]))
        if line.lstrip().startswith("phas"):
            phass.append(float(line.split()[2]))
        if line.lstrip().startswith("ampl"):
            ampls.append(float(line.split()[2]))
        if line.lstrip().startswith("fwhm"):
            fwhms.append(float(line.split()[2]))
    if not (len(phass) == len(ampls) == len(fwhms)):
        print("Number of phases, amplitudes, and FWHMs are " +
              "not the same in '%s'!" % inFile)
        return None
    for ii in range(len(ampls)):
        sigma = fwhms[ii] / (2*np.sqrt(2*np.log(2)))
        primitives += [LCGaussian(p=[ampls[ii], sigma, phass[ii]])]
    return primitives
